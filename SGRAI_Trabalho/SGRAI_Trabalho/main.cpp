#include <stdio.h>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <GL\glut.h>
#include <AL/alut.h>
#include "mathlib.h"
#include "studio.h"
#include "mdlviewer.h"
#include "timer.h"
#include <time.h>
#include <sstream>

#ifdef _WIN32
#include <GL/glaux.h>
#endif

#define finish (i >= 55 && i<=75) && (j>=35 && j<=44)

#pragma comment (lib, "glaux.lib") /* link with Win32 GLAUX lib usada para ler bitmaps */


extern "C" int read_JPEG_file(char *, char **, int *, int *, int *);

#ifdef _WIN32
#define NOME_TEXTURA_CUBOS        "textura_predio.bmp"
#else
#define NOME_TEXTURA_CUBOS        "textura_predio.jpg"
#endif

#define NOME_TEXTURA_CHAO         "chao.jpg"
#define NOME_TEXTURA_META		  "meta.jpg"

#define NOME_TEXTURA_RODA		  "roda.jpg"
#define NOME_TEXTURA_CARRO		  "carro.jpg"

#define NUM_TEXTURAS              5
#define ID_TEXTURA_CUBOS          0
#define ID_TEXTURA_CHAO           1
#define ID_TEXTURA_META			  2
#define ID_TEXTURA_RODA			  3
#define ID_TEXTURA_CARRO		  4

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#define RAD(x)          (M_PI*(x)/180)
#define GRAUS(x)        (180*(x)/M_PI)

#define DELAY_MOVIMENTO     20

#define DEBUG               1

#define MUNDO				100

#define CARRO_LARGURA		5
#define CARRO_COMPRIMENTO	10
#define CARRO_ALTURA		1
#define ALTURA_FAROIS		3

#define TRACK_HEIGHT		18
#define TRACK_WIDTH			18

#define SCALE_CAR         0.04
char nomeMDLcarro[100] = "car.mdl";

typedef struct {
	GLboolean up, down, left, right;
}Teclas;

typedef struct {
	GLfloat x, y, z;
}Pos;

typedef struct {
	Pos eye, center, up;
	GLfloat fov, rot;
}Camera;

typedef struct {
	ALuint buffer, source;
	ALboolean tecla;
} Sound;

typedef struct {
	GLuint vista;
	Teclas teclas;
	Camera camera;
	GLboolean doubleBuffer;
	GLint delayMovimento;
	GLboolean  debug;
	Sound crash, doh, horn, race;
	int xMouse, yMouse;
	int mouseState;
}Estado;

//typedef struct {
//	GLfloat x, y;
//	GLfloat velocidade;
//	GLfloat direcao;
//	GLfloat direcaoRodas;
//}Carro;

typedef struct objecto_t {
	Pos posicao;
	GLfloat direcao;
	GLfloat direccaoRodas;
	GLfloat	velocidade;
}objecto_t;

typedef struct {
	//Carro carro;
	StudioModel car;
	objecto_t obj;
	GLboolean andar;
	GLuint	texID[NUM_TEXTURAS];
}Modelo;

typedef struct {
	GLboolean menuActivo;
	int pontuacao;
	int melhorPontuacao;
}Menu;

Estado estado;
Modelo modelo;
Menu menu;
float resolucaoX;
float resolucaoY;
GLboolean jogoActivo = GL_FALSE;
boolean terminou = false;
GLfloat dir = 1.0f, left = 1.0f;

time_t start;

char trackdata[TRACK_HEIGHT][TRACK_WIDTH + 1] = {
	" **************** ",
	" *       *       *",
	" *       **      *",
	" *   **  *   *   *",
	" *   *   * * * * *",
	" * * *   *   * * *",
	" *   * * *   *   *",
	" *   * * *  **   *",
	" **  * * *   ** **",
	" *   *   **  *   *",
	" *  **   *   *   *",
	" *   ** **   *   *",
	" *   *   *   * * *",
	" *   *   * *** * *",
	" * ***   *   *   *",
	" *   *      *    *",
	" *   *      *    *",
	" **************** "
};

void iniciaModelo() {
	modelo.obj.posicao.x = 65;
	modelo.obj.posicao.y = -60;
	modelo.obj.velocidade = 0;
	modelo.obj.direcao = 180;
	estado.camera.rot = 180;
	modelo.obj.direccaoRodas = 0;
	start = -1;
	jogoActivo = GL_TRUE;
	terminou = false;
	dir = 1.0f;
	left = 1.0f;
}

void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat normal[])
{
	glBegin(GL_POLYGON);
	glNormal3fv(normal);
	glTexCoord2f(1, 1);
	glVertex3fv(a);
	glTexCoord2f(0, 1);
	glVertex3fv(b);
	glTexCoord2f(0, 0);
	glVertex3fv(c);
	glTexCoord2f(1, 0);
	glVertex3fv(d);
	glEnd();
}
void desenhaCubo()
{
	GLfloat vertices[][3] = { { -0.5,-0.5,-0.5 },
	{ 0.5,-0.5,-0.5 },
	{ 0.5,0.5,-0.5 },
	{ -0.5,0.5,-0.5 },
	{ -0.5,-0.5,0.5 },
	{ 0.5,-0.5,0.5 },
	{ 0.5,0.5,0.5 },
	{ -0.5,0.5,0.5 } };
	GLfloat cores[][3] = { { 0.0,1.0,1.0 },
	{ 1.0,0.0,0.0 },
	{ 1.0,1.0,0.0 },
	{ 0.0,1.0,0.0 },
	{ 1.0,0.0,1.0 },
	{ 0.0,0.0,1.0 },
	{ 1.0,1.0,1.0 } };
	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], cores[0]);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], cores[1]);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], cores[2]);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], cores[3]);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], cores[4]);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], cores[5]);
}
void desenhaTanque(/*Tanque t*/)
{
	GLUquadric *quad;
	quad = gluNewQuadric();
	glPushMatrix();
	// rodas
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, modelo.texID[ID_TEXTURA_RODA]);
	glTranslatef(CARRO_LARGURA / 2 + 0.5, CARRO_COMPRIMENTO / 2 - 1.5, 1);
	glRotatef(modelo.obj.direccaoRodas, 0, 0, 1);
	glRotatef(90, 0, 1, 0);
	gluCylinder(quad, 0.5, 0.5, 0.5, 20, 2);
	glBindTexture(GL_TEXTURE_2D, NULL);
	glPopMatrix();
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, modelo.texID[ID_TEXTURA_RODA]);
	glTranslatef(CARRO_LARGURA / 2 + 0.5, -CARRO_COMPRIMENTO / 2 + 1.5, 1);
	glRotatef(90, 0, 1, 0);
	gluCylinder(quad, 0.5, 0.5, 0.5, 20, 2);
	glBindTexture(GL_TEXTURE_2D, NULL);
	glPopMatrix();
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, modelo.texID[ID_TEXTURA_RODA]);
	glTranslatef(-CARRO_LARGURA / 2 - 1, CARRO_COMPRIMENTO / 2 - 1.5, 1);
	glRotatef(modelo.obj.direccaoRodas, 0, 0, 1);
	glRotatef(90, 0, 1, 0);
	gluCylinder(quad, 0.5, 0.5, 0.5, 20, 2);
	glBindTexture(GL_TEXTURE_2D, NULL);
	glPopMatrix();
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, modelo.texID[ID_TEXTURA_RODA]);
	glTranslatef(-CARRO_LARGURA / 2 - 1, -CARRO_COMPRIMENTO / 2 + 1.5, 1);
	glRotatef(90, 0, 1, 0);
	gluCylinder(quad, 0.5, 0.5, 0.5, 20, 2);
	glBindTexture(GL_TEXTURE_2D, NULL);
	glPopMatrix();
	// adornamentos
	glRotatef(modelo.obj.velocidade * 6, 1, 0, 0);
	glRotatef(modelo.obj.direccaoRodas*modelo.obj.velocidade * 2, 0, 1, 0);
	// base
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, modelo.texID[ID_TEXTURA_CARRO]);
	glScalef(CARRO_LARGURA, CARRO_COMPRIMENTO, CARRO_ALTURA + 2);
	desenhaCubo();
	glBindTexture(GL_TEXTURE_2D, NULL);
	glPopMatrix();

	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, modelo.texID[ID_TEXTURA_CARRO]);
	glTranslatef(CARRO_LARGURA / 2 - 0.5, CARRO_COMPRIMENTO / 2 - 1, 1);
	glScalef(CARRO_LARGURA/5, CARRO_COMPRIMENTO/10, CARRO_ALTURA+1);
	desenhaCubo();
	glBindTexture(GL_TEXTURE_2D, NULL);
	glPopMatrix();

	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, modelo.texID[ID_TEXTURA_CARRO]);
	glTranslatef(-CARRO_LARGURA / 2 + 0.5, CARRO_COMPRIMENTO / 2 - 1, 1);
	glScalef(CARRO_LARGURA / 5, CARRO_COMPRIMENTO / 10, CARRO_ALTURA+1);
	desenhaCubo();
	glBindTexture(GL_TEXTURE_2D, NULL);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0, 0, CARRO_ALTURA+1);
	glutSolidSphere(1, 20, 20);
	glPopMatrix();

	glPopMatrix();
}

void desenha_carro()
{
	glTranslatef(modelo.obj.posicao.x, modelo.obj.posicao.y, modelo.obj.posicao.z);
	glRotatef(modelo.obj.direcao, 0, 0, 1);
	//glScalef(SCALE_CAR, SCALE_CAR, SCALE_CAR);
	//mdlviewer_display(modelo.car);
	R_Clear();
	desenhaTanque();
}

void Init(void) {
	srand((unsigned)time(NULL));

	estado.debug = DEBUG;
	estado.delayMovimento = DELAY_MOVIMENTO;

	estado.camera.eye.x = 20;
	estado.camera.eye.y = 0;
	estado.camera.eye.z = 40;
	estado.camera.center.x = modelo.obj.posicao.x;
	estado.camera.center.y = modelo.obj.posicao.y;
	estado.camera.center.z = modelo.obj.posicao.z;
	estado.camera.up.x = 0;
	estado.camera.up.y = 0;
	estado.camera.up.z = 1;
	estado.camera.fov = 60;

	estado.teclas.up = GL_FALSE;
	estado.teclas.down = GL_FALSE;
	estado.teclas.left = GL_FALSE;
	estado.teclas.right = GL_FALSE;

	estado.mouseState = GLUT_UP;

	iniciaModelo();
	modelo.andar = GL_FALSE;

	//glClearColor(0.0, 0.0, 0.0, 0.0);

	menu.menuActivo = GL_TRUE;
}

void init_enables(void) {

	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);

	glEnable(GL_LIGHTING);

	// Luz fraca ambiente
	GLfloat light_diffuse[] = { 0.1f, 0.1f, 0.1f, 1.0 };
	GLfloat _light_position[] = { 75.0, 75.0, 30.0, 1.0 };
	GLfloat _spotlight_position[] = { 0.0f, 0.0f, -1.0f };

	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, _light_position);
	glEnable(GL_LIGHT0);

	glEnable(GL_NORMALIZE);
}

void vistaTopo()
{
	float theta = 1;
	estado.camera.eye.x = modelo.obj.posicao.x + theta*cos(RAD(estado.camera.rot - 90));
	estado.camera.eye.y = modelo.obj.posicao.y + theta*sin(RAD(estado.camera.rot - 90));
	estado.camera.eye.z = modelo.obj.posicao.z + 50;
	estado.camera.center.x = modelo.obj.posicao.x;
	estado.camera.center.y = modelo.obj.posicao.y;
	estado.camera.center.z = modelo.obj.posicao.z;

}

void vistaTerceiraPessoa()
{
	float theta = CARRO_COMPRIMENTO / 2 - 1;
	estado.camera.eye.x = modelo.obj.posicao.x + theta*cos(RAD(estado.camera.rot - 90));
	estado.camera.eye.y = modelo.obj.posicao.y + theta*sin(RAD(estado.camera.rot - 90));
	estado.camera.eye.z = modelo.obj.posicao.z + 7;
	estado.camera.center.x = modelo.obj.posicao.x;
	estado.camera.center.y = modelo.obj.posicao.y;
	estado.camera.center.z = (estado.camera.eye.z + modelo.obj.posicao.z) / 2 + 1;

}

void vistaPrimeiraPessoa() {
	float theta = 1.3;
	estado.camera.eye.x = modelo.obj.posicao.x + theta*cos(RAD(estado.camera.rot + 90));
	estado.camera.eye.y = modelo.obj.posicao.y + theta*sin(RAD(estado.camera.rot + 90));
	estado.camera.eye.z = modelo.obj.posicao.z + 3;
	estado.camera.center.x = estado.camera.eye.x + cos(RAD(estado.camera.rot + 90));
	estado.camera.center.y = estado.camera.eye.y + sin(RAD(estado.camera.rot + 90));
	estado.camera.center.z = modelo.obj.posicao.z + 3;
}

void Reshape(int width, int height) {
	glViewport(0, 0, (GLint)width, (GLint)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(estado.camera.fov, (GLfloat)width / height, 1, 100);
	resolucaoX = (GLfloat)glutGet(GLUT_WINDOW_WIDTH) / 640;
	resolucaoY = (GLfloat)glutGet(GLUT_WINDOW_HEIGHT) / 480;
	glMatrixMode(GL_MODELVIEW);
}

#define STEP    1

void desenhaMundoGeral(GLuint texIDChao, GLuint texIDMeta) {

	GLfloat i, j;
	glBindTexture(GL_TEXTURE_2D, texIDChao);

	for (i = -MUNDO; i <= MUNDO; i += STEP)
		for (j = -MUNDO; j <= MUNDO; j += STEP)
		{
			if (finish) {
				glBindTexture(GL_TEXTURE_2D, texIDMeta);
				glBegin(GL_POLYGON);
				glNormal3f(0, 0, 1);
				glTexCoord2f(0.33, 0.33);
				glVertex3f(i + STEP, j + STEP, 0);
				glTexCoord2f(0, 0.33);
				glVertex3f(i, j + STEP, 0);
				glTexCoord2f(0, 0);
				glVertex3f(i, j, 0);
				glTexCoord2f(0.33, 0);
				glVertex3f(i + STEP, j, 0);
				glEnd();
				glBindTexture(GL_TEXTURE_2D, texIDChao);
			}
			else {
				glBegin(GL_POLYGON);
				glNormal3f(0, 0, 1);
				glTexCoord2f(1, 1);
				glVertex3f(i + STEP, j + STEP, 0);
				glTexCoord2f(0, 1);
				glVertex3f(i, j + STEP, 0);
				glTexCoord2f(0, 0);
				glVertex3f(i, j, 0);
				glTexCoord2f(1, 0);
				glVertex3f(i + STEP, j, 0);
				glEnd();
			}
		}
	glBindTexture(GL_TEXTURE_2D, NULL);
}


void desenhaObstaculo(GLuint texID)
{
	GLfloat vertices[][3] = { { -0.5,-0.5,-0.5 },
	{ 0.5,-0.5,-0.5 },
	{ 0.5,0.5,-0.5 },
	{ -0.5,0.5,-0.5 },
	{ -0.5,-0.5,0.5 },
	{ 0.5,-0.5,0.5 },
	{ 0.5,0.5,0.5 },
	{ -0.5,0.5,0.5 } };
	GLfloat normais[][3] = { { 0,0,-1 },
	{ 0,1,0 },
	{ -1,0,0 },
	{ 1,0,0 },
	{ 0,0,1 },
	{ 0,-1,0 } };

	glBindTexture(GL_TEXTURE_2D, texID);

	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0]);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[1]);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[2]);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[3]);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[5]);

	glBindTexture(GL_TEXTURE_2D, NULL);

	//glColor3f(0.3, 0.3, 0.3);
	//Fora do bind para n�o colocar textura na parte de cima dos predios.
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[4]);
}

void desenhaPista(GLuint texID)
{
	int i, j;
	glColor3f(0.8f, 0.8f, 0.8f);
	glPushMatrix();
	glTranslatef(-TRACK_HEIGHT*0.5, -TRACK_WIDTH*0.5, 0.5);
	for (i = 0; i < TRACK_HEIGHT; i++)
		for (j = 0; j < TRACK_WIDTH; j++)
			if (trackdata[i][j] == '*')
			{
				glPushMatrix();
				glTranslatef(i, j, 0);
				desenhaObstaculo(texID);
				glPopMatrix();
			}
	glPopMatrix();
}

void escreverTexto(int x, int y, float r, float g, float b, char *string)
{
	glColor3f(r, g, b);
	glRasterPos2f(x, y);
	int len, i;
	len = (int)strlen(string);
	for (i = 0; i < len; i++) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, string[i]);
	}
}

void desenhaBotao(float xini, float yini, float xfim, float yfim, float cor[3])
{
	glColor3fv(cor);
	glBegin(GL_QUADS);
	glVertex2f(xini, yini);
	glVertex2f(xini, yfim);
	glVertex2f(xfim, yfim);
	glVertex2f(xfim, yini);
	glEnd();
}

void desenhaMenu()
{
	glMatrixMode(GL_PROJECTION);

	glPushMatrix();

	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_INIT_WINDOW_WIDTH), 0, glutGet(GLUT_INIT_WINDOW_HEIGHT));
	glMatrixMode(GL_MODELVIEW);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glPushMatrix();

	glLoadIdentity();

	float branco[3] = { 1.0, 1.0, 1.0 };
	float cinzento[3] = { 0.3f, 0.3f, 0.3f };
	std::string nome = "Corrida de Carros";
	char* p = new char[nome.length() + 1];

	p = new char[nome.length() + 1];
	memcpy(p, nome.c_str(), nome.length() + 1);

	escreverTexto(222, 350, 1, 1, 1, p);

	std::string sair = "Sair";
	char* s = new char[sair.length() + 1];
	memcpy(s, sair.c_str(), sair.length() + 1);
	desenhaBotao(350, 90, 450, 130, cinzento);
	escreverTexto(385, 102, 1, 1, 1, s);

	std::string jogar = "Jogar";
	char* j = new char[jogar.length() + 1];
	memcpy(j, jogar.c_str(), jogar.length() + 1);
	desenhaBotao(150, 90, 250, 130, cinzento);
	escreverTexto(175, 102, 1, 1, 1, j);

	glPopMatrix();

	glMatrixMode(GL_PROJECTION);

	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
}

void farolDir() {
	GLfloat light_ambient[] = { dir, dir, dir, 1.0 };
	GLfloat light_diffuse[] = { dir, dir, dir, 1.0 };
	GLfloat light_specular[] = { dir, dir, dir, 1.0 };
	GLfloat _light_position[] = { 0.0, 0.0, 0.0, 1.0 };
	GLfloat _spotlight_position[] = { 0.1f, 1.0f, 0.0f };

	glLightfv(GL_LIGHT2, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT2, GL_SPECULAR, light_specular);

	glLightfv(GL_LIGHT2, GL_POSITION, _light_position);
	glLightf(GL_LIGHT2, GL_SPOT_CUTOFF, 10.0);
	glLightf(GL_LIGHT2, GL_SPOT_EXPONENT, 2.0);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, _spotlight_position);
	glEnable(GL_LIGHT2);
}

void farolEsq() {
	GLfloat light_ambient[] = { left, left, left, 1.0 };
	GLfloat light_diffuse[] = { left, left, left, 1.0 };
	GLfloat light_specular[] = { left, left, left, 1.0 };
	GLfloat _light_position[] = { 0.0, 0.0, 0.0, 1.0 };
	GLfloat _spotlight_position[] = { -0.1f, 1.0f, 0.0f };

	glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);

	glLightfv(GL_LIGHT1, GL_POSITION, _light_position);
	glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 10.0);
	glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 2.0);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, _spotlight_position);
	glEnable(GL_LIGHT1);
}

void Draw(void) {

	if (menu.menuActivo)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		desenhaMenu();
	}
	else {

		init_enables();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();

		gluLookAt(estado.camera.eye.x, estado.camera.eye.y, estado.camera.eye.z,
			estado.camera.center.x, estado.camera.center.y, estado.camera.center.z,
			estado.camera.up.x, estado.camera.up.y, estado.camera.up.z);

		switch (estado.vista) {
		case 0:
			vistaTopo();
			break;
		case 1:
			vistaTerceiraPessoa();
			break;
		case 2:
			vistaPrimeiraPessoa();
			break;
		}


		glColor3ub(98, 142, 88);
		glPushMatrix();
		desenhaMundoGeral(modelo.texID[ID_TEXTURA_CHAO], modelo.texID[ID_TEXTURA_META]);
		glPopMatrix();

		glPushMatrix();
		glScalef(10, 10, 10);
		desenhaPista(modelo.texID[ID_TEXTURA_CUBOS]);
		glPopMatrix();

		glPushMatrix();
		desenha_carro();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(modelo.obj.posicao.x, modelo.obj.posicao.y, 0);
		glRotatef(modelo.obj.direcao, 0, 0, 1);
		glTranslatef(CARRO_LARGURA / 2.0 - 2.5, -CARRO_COMPRIMENTO - 2, ALTURA_FAROIS);
		//right headlight
		farolDir();
		glTranslatef(-CARRO_LARGURA + 5.0, 0, 0);
		//left headlight
		farolEsq();
		glTranslatef(0, -CARRO_COMPRIMENTO, 0);
		//left taillight
		glTranslatef(CARRO_LARGURA, 0, 0);
		//right taillight
		glPopMatrix();

		if (terminou) {
			glDisable(GL_LIGHTING);
			glPushMatrix();
			glTranslatef(modelo.obj.posicao.x, modelo.obj.posicao.y, 30);
			glRotatef(estado.camera.rot, 0, 0, 1);
			glTranslatef(-2, 5, 0);
			std::string nome = "Terminou!";
			char* p = new char[nome.length() + 1];

			memcpy(p, nome.c_str(), nome.length() + 1);
			glScalef(10, 10, 10);
			escreverTexto(0, 0, 1, 1, 1, p);
			glScalef(0.1, 0.1, 0.1);
			glTranslatef(-1.5, -3, 0);

			std::ostringstream convert;
			int s = menu.pontuacao;
			int h = s / 360;
			s -= h * 360;
			int m = s / 60;
			s -= m * 60;

			std::string tempostr = "Tempo: ";
			if (h < 10) {
				convert << "0";
			}
			convert << h;
			convert << ":";
			if (m < 10) {
				convert << "0";
			}
			convert << m;
			convert << ":";
			if (s < 10) {
				convert << "0";
			}
			convert << s;

			tempostr.append(convert.str());

			char* str = new char[tempostr.length() + 1];

			memcpy(str, tempostr.c_str(), tempostr.length() + 1);
			glScalef(10, 10, 10);
			escreverTexto(0, 0, 1, 1, 1, str);
			glScalef(0.1, 0.1, 0.1);
			glTranslatef(-8.5, -10, 0);
			nome = "Prima 'R' para reiniciar ou 'ENTER' para regressar ao menu.";
			p = new char[nome.length() + 1];

			memcpy(p, nome.c_str(), nome.length() + 1);
			glScalef(5, 5, 5);
			escreverTexto(0, 0, 1, 1, 1, p);
			glScalef(0.1, 0.1, 0.1);

			glTranslatef(13, 10, 0);

			int minutes = menu.melhorPontuacao / 60;
			int seconds = menu.melhorPontuacao % 60;
			int hours = minutes / 60;
			minutes = minutes % 60;

			std::ostringstream convMelhorPontuacao;
			std::string melhorstr = "Melhor Pontuacao: ";
			if (hours < 10) {
				convMelhorPontuacao << "0";
			}
			convMelhorPontuacao << hours;
			convMelhorPontuacao << ":";
			if (minutes < 10) {
				convMelhorPontuacao << "0";
			}
			convMelhorPontuacao << minutes;
			convMelhorPontuacao << ":";
			if (seconds  < 10) {
				convMelhorPontuacao << "0";
			}
			convMelhorPontuacao << seconds;

			melhorstr.append(convMelhorPontuacao.str());

			p = new char[melhorstr.length() + 1];

			memcpy(p, melhorstr.c_str(), melhorstr.length() + 1);
			glScalef(5, 5, 5);
			escreverTexto(0, 0, 1, 1, 1, p);
			glScalef(0.1, 0.1, 0.1);

			glPopMatrix();
		}
	}
	glFlush();
	if (estado.doubleBuffer)
		glutSwapBuffers();
}

GLboolean checkCrash(GLfloat nx, GLfloat ny) {
	GLfloat x0 = nx / 10;
	GLfloat y0 = ny / 10;
	int x = roundf(x0 + TRACK_WIDTH / 2.0);
	int y = roundf(y0 + TRACK_HEIGHT / 2.0);
	if (!(x<0 || x>TRACK_WIDTH || y < 0 || y >= TRACK_HEIGHT)) {
		if (trackdata[x][y] == '*') {
			return GL_TRUE;
		}
	}
	return GL_FALSE;
}

GLfloat calcX(GLfloat nx, GLfloat r, GLfloat angle) {
	return nx + r * cos(angle);
}

GLfloat calcY(GLfloat ny, GLfloat r, GLfloat angle) {
	return ny + r * sin(angle);
}

GLboolean sideCalc(float rightUpX, float rightUpY, float leftUpX, float leftUpY, float rightDownX, float rightDownY, float leftDownX, float leftDownY) {
	int count = 20;
	float dC = (float)CARRO_COMPRIMENTO / count;
	float dL = (float)CARRO_LARGURA / count;

	float theta = atan2(rightUpY - leftUpY, rightUpX - leftUpX);
	for (int i = 0; i <= count; i++) {
		float x = leftUpX + i*dL*cos(theta);
		float y = leftUpY + i*dL*sin(theta);
		if (checkCrash(x, y)) {
			if (i <= 5 && left > 0.01) {
				left -= 0.02;
			}
			else if (i >= 15 && dir > 0.01) {
				dir -= 0.02;
			}
			else {
				if (dir > 0.01 && left > 0.01) {
					left -= 0.02;
					dir -= 0.02;
				}
			}
			return GL_TRUE;
		}
	}

	theta = atan2(rightDownY - leftDownY, rightDownX - leftDownX);
	for (int i = 0; i <= count; i++) {
		float x = leftDownX + i*dL*cos(theta);
		float y = leftDownY + i*dL*sin(theta);
		if (checkCrash(x, y)) {
			return GL_TRUE;
		}
	}

	theta = atan2(rightUpY - rightDownY, rightUpX - rightDownX);
	for (int i = 0; i <= count; i++) {
		float x = rightDownX + i*dC*cos(theta);
		float y = rightDownY + i*dC*sin(theta);
		if (checkCrash(x, y)) {
			return GL_TRUE;
		}
	}

	theta = atan2(leftUpY - leftDownY, leftUpX - leftDownX);
	for (int i = 0; i <= count; i++) {
		float x = leftDownX + i*dC*cos(theta);
		float y = leftDownY + i*dC*sin(theta);
		if (checkCrash(x, y)) {
			return GL_TRUE;
		}
	}

	return GL_FALSE;
}

GLboolean calc(GLfloat nx, GLfloat ny, float r, float angle, float at) {
	float theta = angle - at;
	float rUx = calcX(nx, r, theta);
	float rUy = calcY(ny, r, theta);

	theta = angle + at;
	float lUx = calcX(nx, r, theta);
	float lUy = calcY(ny, r, theta);

	theta = angle + M_PI + at;
	float rDx = calcX(nx, r, theta);
	float rDy = calcY(ny, r, theta);

	theta = angle + M_PI - at;
	float lDx = calcX(nx, r, theta);
	float lDy = calcY(ny, r, theta);

	return sideCalc(rUx, rUy, lUx, lUy, rDx, rDy, lDx, lDy);
}

GLboolean detectaColisao(GLfloat nx, GLfloat ny, GLfloat direcao)
{
	float r = sqrt(pow(CARRO_COMPRIMENTO, 2) + pow(CARRO_LARGURA, 2)) / 2.0;
	float angle = RAD(direcao + 90);
	float at = atan((float)CARRO_LARGURA / CARRO_COMPRIMENTO);

	return calc(nx, ny, r, angle, at);
}

void checkFinish(float i, float j) {
	if (finish) {
		estado.vista = 0;
		terminou = true;
		jogoActivo = false;
		ALint state;
		alGetSourcei(estado.race.source, AL_SOURCE_STATE, &state);
		alSourceStop(estado.race.source);
		menu.pontuacao = difftime(time(0), start);
		if (menu.pontuacao < menu.melhorPontuacao || menu.melhorPontuacao == 0) {
			menu.melhorPontuacao = menu.pontuacao;
		}
	}
}

void Timer(int value) {

	glutTimerFunc(estado.delayMovimento, Timer, 0);

	if (menu.menuActivo) {
		if (estado.xMouse > 150 * resolucaoX && estado.xMouse < 250 * resolucaoX && estado.yMouse > 350 * resolucaoY && estado.yMouse < 390 * resolucaoY && estado.mouseState == 0)
		{
			menu.menuActivo = GL_FALSE;
			jogoActivo = GL_TRUE;
		}

		if (estado.xMouse > 350 * resolucaoX && estado.xMouse < 450 * resolucaoX && estado.yMouse > 350 * resolucaoY && estado.yMouse < 390 * resolucaoY && estado.mouseState == 0)
		{
			exit(1);
		}
	}if (!terminou) {
		checkFinish(modelo.obj.posicao.x, modelo.obj.posicao.y);

		ALint state;

		alGetSourcei(estado.horn.source, AL_SOURCE_STATE, &state);
		if (estado.horn.tecla)
		{
			if (state != AL_PLAYING)
				alSourcePlay(estado.horn.source);
		}

		GLboolean andar = GL_FALSE;

		//acelerar e travar
		if (estado.teclas.up && modelo.obj.velocidade < 0.6) {
			modelo.obj.velocidade += 0.02;
			ALint state;
			alGetSourcei(estado.race.source, AL_SOURCE_STATE, &state);
				alSourcePlay(estado.race.source);
		}
		if (estado.teclas.down && modelo.obj.velocidade > -0.6) {
			modelo.obj.velocidade -= 0.02;
		}

		//perder velocidade quando nao se est� acelerar
		if (!estado.teclas.up && !estado.teclas.down && modelo.obj.velocidade != 0) {
			if (modelo.obj.velocidade > 0)
				modelo.obj.velocidade -= 0.01;
			else
				modelo.obj.velocidade += 0.01;
			if (modelo.obj.velocidade<0.01 && modelo.obj.velocidade>-0.01) {
				modelo.obj.velocidade = 0;
			}
		}

		//alterar direcao para posicao normal quando tecla esquerda ou direita nao estiver a ser pressionada
		if (!estado.teclas.right && !estado.teclas.left && modelo.obj.velocidade != 0 && modelo.obj.direccaoRodas != 0) {
			if (modelo.obj.direccaoRodas > 0) {
				modelo.obj.direccaoRodas -= 1;
			}
			else {
				modelo.obj.direccaoRodas += 1;
			}

			if (modelo.obj.direccaoRodas > -0.05 && modelo.obj.direccaoRodas < 0.05)
				modelo.obj.direccaoRodas = 0;
		}

		//direcao das rodas
		if (estado.teclas.left && modelo.obj.direccaoRodas < 15) {
			modelo.obj.direccaoRodas += 1;
		}
		if (estado.teclas.right && modelo.obj.direccaoRodas > -15) {
			modelo.obj.direccaoRodas -= 1;
		}

		GLfloat direcao = modelo.obj.direcao + (modelo.obj.direccaoRodas / 3)*modelo.obj.velocidade;
		GLfloat nx = modelo.obj.posicao.x + modelo.obj.velocidade*cos(RAD(direcao + 90));
		GLfloat ny = modelo.obj.posicao.y + modelo.obj.velocidade*sin(RAD(direcao + 90));


		if (!detectaColisao(nx, ny, direcao)) {
			if (start < 0) {
				start = time(0);
			}
			modelo.obj.direcao = direcao;
			estado.camera.rot = direcao;
			modelo.obj.posicao.x = nx;
			modelo.obj.posicao.y = ny;
			modelo.andar = GL_TRUE;
		}
		else {
			modelo.obj.velocidade = 0;
			if (modelo.andar) {
				ALint state;
				alGetSourcei(estado.crash.source, AL_SOURCE_STATE, &state);
				alSourcePlay(estado.crash.source);
				modelo.andar = GL_FALSE;

				alGetSourcei(estado.doh.source, AL_SOURCE_STATE, &state);
				alSourcePlay(estado.doh.source);
				modelo.andar = GL_FALSE;
			}
		}

		glutPostRedisplay();
	}
	else {
		estado.camera.rot++;
		glutPostRedisplay();
	}
}

void imprime_ajuda(void)
{
	printf("\n\nCorrida de Carros - SGRAI\n\n");
	printf("H/h - Ajuda!\n");
	printf("Up  - Acelera \n");
	printf("Down- Trava \n");
	printf("Left- Virar para a direita\n");
	printf("Right- Virar para a esquerda\n");
	printf("B/b - Buzinar\n");
	printf("F1 - Vista anterior\n");
	printf("F2- Vista seguinte\n");
	printf("R/r - Reinicia modelo\n");
	printf("Esc - Fecha o jogo\n\n");

}

void Key(unsigned char key, int x, int y)
{
	if (menu.menuActivo && key != 'h' && key != 'H') {
		return;
	}
	switch (key) {
	case 13:
		if (terminou) {

			jogoActivo = GL_FALSE;
			Init();
		}
		break;
	case 27:
		exit(1);
	case 'b':
	case 'B':
		estado.horn.tecla = AL_TRUE;
		break;
	case 'h':
	case 'H':
		imprime_ajuda();
		break;
	case 'r':
	case 'R':
		iniciaModelo();
		break;
	}
	if (estado.debug)
		printf("Carregou na tecla %c\n", key);
}

void KeyUp(unsigned char key, int x, int y)
{
	if (menu.menuActivo) {
		return;
	}
	switch (key) {
	case 'B':
	case 'b':
		estado.horn.tecla = AL_FALSE;
		break;
	}

	if (estado.debug)
		printf("Largou a tecla %c\n", key);
}

void SpecialKey(int key, int x, int y)
{
	if (menu.menuActivo) {
		return;
	}
	switch (key) {
	case GLUT_KEY_F1: estado.vista = estado.vista == 0 ? 2 : estado.vista - 1;
		break;
	case GLUT_KEY_F2: estado.vista = estado.vista == 2 ? 0 : estado.vista + 1;
		break;
	case GLUT_KEY_RIGHT:
		estado.teclas.right = GL_TRUE;
		break;
	case GLUT_KEY_LEFT:
		estado.teclas.left = GL_TRUE;
		break;
	case GLUT_KEY_UP:
		estado.teclas.up = GL_TRUE;
		break;
	case GLUT_KEY_DOWN:
		estado.teclas.down = GL_TRUE;
		break;
	}

	if (estado.debug)
		printf("Carregou na tecla especial %d\n", key);
}

void SpecialKeyUp(int key, int x, int y)
{
	if (menu.menuActivo) {
		return;
	}
	switch (key) {
	case GLUT_KEY_RIGHT:
		estado.teclas.right = GL_FALSE;
		break;
	case GLUT_KEY_LEFT:
		estado.teclas.left = GL_FALSE;
		break;
	case GLUT_KEY_UP:
		estado.teclas.up = GL_FALSE;
		break;
	case GLUT_KEY_DOWN:
		estado.teclas.down = GL_FALSE;
		break;
	}

}

void InitAudio() {
	estado.doh.buffer = alutCreateBufferFromFile("sound/doh.wav");
	alGenSources(1, &estado.doh.source);
	alSourcei(estado.doh.source, AL_BUFFER, estado.doh.buffer);
	estado.doh.tecla = AL_FALSE;

	estado.crash.buffer = alutCreateBufferFromFile("sound/crash.wav");
	alGenSources(1, &estado.crash.source);
	alSourcei(estado.crash.source, AL_BUFFER, estado.crash.buffer);
	estado.crash.tecla = AL_FALSE;

	estado.horn.buffer = alutCreateBufferFromFile("sound/horn.wav");
	alGenSources(1, &estado.horn.source);
	alSourcei(estado.horn.source, AL_BUFFER, estado.horn.buffer);
	estado.horn.tecla = AL_FALSE;

	estado.race.buffer = alutCreateBufferFromFile("sound/race.wav");
	alGenSources(1, &estado.race.source);
	alSourcei(estado.race.source, AL_BUFFER, estado.race.buffer);
	estado.race.buffer = AL_FALSE;
}

#ifdef _WIN32

AUX_RGBImageRec *LoadBMP(char *Filename)				// Loads A Bitmap Image
{
	FILE *File = NULL;									// File Handle

	if (!Filename)										// Make Sure A Filename Was Given
	{
		return NULL;									// If Not Return NULL
	}

	File = fopen(Filename, "r");							// Check To See If The File Exists

	if (File)											// Does The File Exist?
	{
		fclose(File);									// Close The Handle
		return auxDIBImageLoad(Filename);				// Load The Bitmap And Return A Pointer
	}

	return NULL;										// If Load Failed Return NULL
}
#endif

void createTextures(GLuint texID[])
{
	char *image;
	int w, h, bpp;

#ifdef _WIN32
	AUX_RGBImageRec *TextureImage[1];					// Create Storage Space For The Texture

	memset(TextureImage, 0, sizeof(void *) * 1);           	// Set The Pointer To NULL
#endif

	glGenTextures(NUM_TEXTURAS, texID);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

#ifdef _WIN32
	if (TextureImage[0] = LoadBMP(NOME_TEXTURA_CUBOS))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CUBOS]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
#else
	if (read_JPEG_file(NOME_TEXTURA_CUBOS, &image, &w, &h, &bpp))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CUBOS]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
#endif
	else
	{
		printf("Textura %s not Found\n", NOME_TEXTURA_CUBOS);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_CHAO, &image, &w, &h, &bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CHAO]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_CHAO);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_RODA, &image, &w, &h, &bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_RODA]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_RODA);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_CARRO, &image, &w, &h, &bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CARRO]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_CARRO);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_META, &image, &w, &h, &bpp)) {
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_META]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}

	glBindTexture(GL_TEXTURE_2D, NULL);
}

void mouse(int btn, int state, int x, int y) {
	switch (btn) {
	case GLUT_RIGHT_BUTTON:
		if (state == GLUT_DOWN) {
			estado.xMouse = x;
			estado.yMouse = y;
			estado.mouseState = 0;
		}
		else {
			glutMotionFunc(NULL);
		}
		break;
	case GLUT_LEFT_BUTTON:
		if (state == GLUT_DOWN) {
			estado.xMouse = x;
			estado.yMouse = y;
			estado.mouseState = 0;
		}
		else {
			glutMotionFunc(NULL);
		}
		break;
	}
}

int main(int argc, char **argv) {
	alutInit(&argc, argv);
	InitAudio();

	estado.doubleBuffer = 1;

	glutInit(&argc, argv);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(640, 480);
	glutInitDisplayMode(((estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGB | GLUT_DEPTH);
	if (glutCreateWindow("Corrida de Carros") == GL_FALSE)
		exit(1);

	Init();

	createTextures(modelo.texID);

	//mdlviewer_init(nomeMDLcarro, modelo.car);

	imprime_ajuda();

	glutReshapeFunc(Reshape);
	glutDisplayFunc(Draw);

	glutKeyboardFunc(Key);
	glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);
	glutMouseFunc(mouse);

	glutTimerFunc(estado.delayMovimento, Timer, 0);

	glutMainLoop();
	return 0;
}
