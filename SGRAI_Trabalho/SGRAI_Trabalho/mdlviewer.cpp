/***
*
*	Copyright (c) 1998, Valve LLC. All rights reserved.
*
*  Last modified 11/26/2001, Michael Haller, haller@fhs-hagenberg.ac.at
*
**/

#include <stdio.h>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include "mathlib.h"
#include "studio.h"
#include "mdlviewer.h"


#pragma warning( disable : 4244 ) // conversion from 'double ' to 'float ', possible loss of data
#pragma warning( disable : 4305 ) // truncation from 'const double ' to 'float '

vec3_t		g_vright;		// needs to be set to viewer's right in order for chrome to work
float		g_lambert = 1.5;

float		gldepthmin = 0;
float		gldepthmax = 10.0;


/*
=============
R_Clear
=============
*/
void R_Clear(void)
{
	glDepthFunc(GL_LEQUAL);
	glDepthRange(gldepthmin, gldepthmax);
	glDepthMask(1);
}

void mdlviewer_display(StudioModel &tempmodel)
{
	R_Clear();

	tempmodel.SetBlending(0, 0.0);
	tempmodel.SetBlending(1, 0.0);

	float curr = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
	tempmodel.AdvanceFrame(curr - tempmodel.GetPrev());
	tempmodel.SetPrev(curr);

	glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT | GL_TEXTURE_BIT);
	tempmodel.DrawModel();
	glPopAttrib();
}


void mdlviewer_init(const char *modelname, StudioModel &tempmodel)
{
	// make a bogus texture
	// R_InitTexture( );

	tempmodel.Init(modelname);
	tempmodel.SetSequence(0);

	tempmodel.SetController(0, 0.0);
	tempmodel.SetController(1, 0.0);
	tempmodel.SetController(2, 0.0);
	tempmodel.SetController(3, 0.0);
	tempmodel.SetMouth(0);
}


void mdlviewer_nextsequence(StudioModel &tempmodel)
{
	int iSeq = tempmodel.GetSequence();
	if (iSeq == tempmodel.SetSequence(iSeq + 1))
	{
		tempmodel.SetSequence(0);
	}

	printf("Seq: %d\n", tempmodel.GetSequence());
}


void mdlviewer_prevsequence(StudioModel &tempmodel)
{
	int iSeq = tempmodel.GetSequence();
	if (iSeq == tempmodel.SetSequence(iSeq - 1))
	{
		tempmodel.SetSequence(0);
	}

	printf("Seq: %d\n", tempmodel.GetSequence());
}
